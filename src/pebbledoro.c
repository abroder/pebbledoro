#include <pebble.h>

#include "pebbledoro.h"

#include "pebble-utils/pebble-utils.h"

#define TIMER_RADIUS 65
#define NUM_PHASES 5
#define KEY_COMPLETED 1

static Window *s_main_window;
static time_t s_start_time;
static int32_t s_completed_pomodoros;

static int s_phase_lengths[] = {
  60 * 25, 60 * 5, 60 * 25, 60 * 5, 60 * 25
};
static int s_current = -1;

static void tick_timer_seconds_handler(struct tm *tick_time, TimeUnits units_changed) {
  time_t curr_time = time(NULL);
  time_t difference = curr_time - s_start_time;

  APP_LOG(APP_LOG_LEVEL_INFO, "Time elapsed in current phase: %d", (int) difference);

  if (units_changed % 30 == 0) {
    APP_LOG(APP_LOG_LEVEL_INFO, "Redrawing the screen");

    layer_mark_dirty(window_get_root_layer(s_main_window));
  }

  if (difference >= s_phase_lengths[s_current]) {
    s_current++;

    if (s_current == NUM_PHASES) {
      s_completed_pomodoros++;
      end_pomodoro();

      APP_LOG(APP_LOG_LEVEL_INFO, "Completed pomodoros: %li", s_completed_pomodoros);
      return;
    }

    s_start_time = curr_time;
    if (s_current % 2 == 0) { // moving into work
      vibes_double_pulse();
    } else { // moving into break
      vibes_short_pulse();
    }

    APP_LOG(APP_LOG_LEVEL_INFO, "Moving on to part %d", s_current);
  }
}

static void start_pomodoro() {
  s_start_time = time(NULL);
  s_current = 0;
  tick_timer_service_subscribe(SECOND_UNIT, tick_timer_seconds_handler);

  APP_LOG(APP_LOG_LEVEL_INFO, "Starting Pomodoro at %d", (int) s_start_time);
}

static void end_pomodoro() {
  s_start_time = 0;
  s_current = -1;
  tick_timer_service_unsubscribe();

  APP_LOG(APP_LOG_LEVEL_INFO, "Stopping Pomodoro at %d", (int) time(NULL));
}

static void toggle_pomodoro() {
  if (s_current != -1) {
    end_pomodoro();
  } else {
    start_pomodoro();
  }
}

static void select_single_click_handler(ClickRecognizerRef recognizer, void *context) {
  toggle_pomodoro();
}

static void main_window_click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_single_click_handler);
}

static void root_layer_update_proc(Layer *layer, GContext *ctx) {
  graphics_context_set_fill_color(ctx, GColorWhite);
  graphics_fill_rect(ctx, GRect(0, 0, layer_get_frame(layer).size.w, layer_get_frame(layer).size.h), 0, GCornerNone);

  GPoint center = {layer_get_frame(layer).size.w / 2, layer_get_frame(layer).size.h / 2};

  graphics_context_set_fill_color(ctx, GColorBlack);

  if (s_current == -1) return;

  if (s_current % 2 != 0) { // draw filled circle
    graphics_fill_circle(ctx, center, TIMER_RADIUS - 1);
    graphics_context_set_fill_color(ctx, GColorWhite);
  }

  time_t curr_time = time(NULL);
  time_t difference = curr_time - s_start_time;

  int32_t max_angle = 360 * difference / s_phase_lengths[s_current];

  graphics_fill_arc(ctx, center, TIMER_RADIUS, max_angle);
}

static void main_window_load() {
  Layer *root_layer = window_get_root_layer(s_main_window);
  layer_set_update_proc(root_layer, root_layer_update_proc);
}

static void main_window_unload() {
}

static void init() {
  s_main_window = window_create();
  window_set_window_handlers(s_main_window, (WindowHandlers) {
    .load = main_window_load,
    .unload = main_window_unload 
  });
  window_set_click_config_provider(s_main_window, main_window_click_config_provider);

  s_completed_pomodoros = persist_read_int(KEY_COMPLETED);

  window_stack_push(s_main_window, true);
}

static void deinit() {
  persist_write_int(KEY_COMPLETED, s_completed_pomodoros);

  window_destroy(s_main_window);
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}
