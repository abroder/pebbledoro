// app lifecycle
static void init();
static void deinit();

static void main_window_load();
static void root_layer_update_proc(Layer *layer, GContext *ctx);
static void main_window_unload();

// click handlers
static void main_window_click_config_provider(void *context);
static void select_single_click_handler(ClickRecognizerRef recognizer, void *context);

// pomodoro logic
static void start_pomodoro();
static void end_pomodoro();
static void toggle_pomodoro();

static void tick_timer_seconds_handler(struct tm *tick_time, TimeUnits units_changed);